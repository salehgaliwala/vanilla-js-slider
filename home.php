<?php get_header();
?>
<div id="slides">
<?php
 $defaults = array(   
    'post_type'        => 'slider',
    'post_status'    => 'publish',
 );
    $posts = get_posts( $defaults);
    $count = sizeof($posts);
    $i=1;
    foreach($posts as $post)
    {  
     $percent =  $i / $count * 100;   

?>
<div class="banner showing">
<h5><?php echo get_the_title($post->ID) ?></h5>
<div class="container">
<div class="row"><div class="col-md-9 col-md-offset-3"><div class="banner-image"><img src="<?php the_field('banner_image') ?>" alt=""></div></div></div>
<div class="banner-content">
<h4><?php echo get_post_meta($post->ID,'sub_heading',true) ?></h4>
<p><?php echo get_post_meta($post->ID,'text',true) ?></p>
<ul class="banner-pagination">
<li>01</li>
<li class="active"><span style="width:<?php echo $percent ?>%"></span></li>
<!--<li><span></span></li>
<li><span></span></li>
<li><span></span></li>-->
<li><?php echo $count ?></li>
</ul>
<a href="#" class="link">Voir plus</a>
<nav class="banner-nav clearfix">
<a  title="Prev" onClick="prevSlide()"><span class="sr-only">Prev</span></a>
<a  title="Next" onClick="nextSlide()"><span class="sr-only">Next</span></a>
</nav>
</div>
<div class="banner-btn"><a href="#">Commandez vos Tableaux</a></div>
</div>
</div>
    <?php $i++;  } ?>
</div>
<script>
 var slides = document.querySelectorAll('#slides .banner');
var currentSlide = 0;
var slideInterval = setInterval(nextSlide,20000);
function nextSlide() {
    slides[currentSlide].className = 'banner';
    currentSlide = (currentSlide+1)%slides.length;
    console.log( currentSlide);
    slides[currentSlide].className = 'banner showing';
}
function prevSlide() {
    console.log( currentSlide);
    if(currentSlide <= 0)
    {
        currentSlide = slides.length-1;
      
    }
    slides[currentSlide].className = 'banner';
    currentSlide = (currentSlide-1)%slides.length;
    console.log( currentSlide);
    slides[currentSlide].className = 'banner showing';
}
</script>
<?php
get_footer();